
import {
  LitElement,
  html,
  css,
} from "https://unpkg.com/lit?module";

export class WhatsUp extends LitElement {
  constructor() {
    super();
  }

  static get styles() {
    return [css`
      button {
        background: #1E88E5;
        color: white;
        padding: 2rem 4rem;
        border: 0;
        font-size: 1.5rem;
      }
    `]
  }

  render() {
    return html`<button @click=${this.handleClick}>What's</button>`;
  }

  handleClick(e) {
    alert("Sup?");
  }
}
window.customElements.define('whats-up', WhatsUp);