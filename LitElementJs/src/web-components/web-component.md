- Custom Elements: (en-US) pour créer et enregistrer de nouveaux éléments HTML et les faire reconnaître par le navigateur.
- HTML Templates: squelette pour créer des éléments HTML instanciables.
- Shadow DOM: permet d'encapsuler le JavaScript et le CSS des éléments.

```js
customElements.define(
  "web-component",
  class extends HTMLElement {
    constructor() {
      super();
    }

    connectedCallback() {
      // Le composant est créé
    }

    adoptedCallback() {
      // Le composant est déplacé
    }

    disconnectedCallback() {
      // Le composant est detruit
      // Attention si le noeud est déplacé alors cette méthode sera appeler puis connectedCallback
    }

    attributeChangedCallback(name, oldValue, newValue) {
      console.log(
        `L'attribut ${name} à changé de valeur de ${oldValue} à ${newValue}`
      );
    }
  }
);
```
